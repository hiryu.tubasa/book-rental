import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import iziToast from 'iziToast'
import 'izitoast/dist/css/iziToast.min.css'

import { default as Web3 } from 'web3'
import { default as contract } from 'truffle-contract'

import adoption_artifacts from '../../build/contracts/Adoption.json'
import user_artifacts from '../../build/contracts/User.json'
import env from '../../env.json'

const Adoption = contract(adoption_artifacts)
const User = contract(user_artifacts)

window.App = {
  start: function () {
    Adoption.setProvider(web3.currentProvider)
    User.setProvider(web3.currentProvider)

    web3.eth.getAccounts((err, accounts) => {
      if (err != null) {
        alert('There was an error fetching your accounts.')
        return
      }
      if (accounts.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.")
        return
      }
      $('#InputUserAddress').val(accounts[0])
      this.initCheckUser(accounts[0])
      this.initBooks()
      return this.bindEvents()
    })
  },

  bindEvents: function () {
    $(document).on('click', '.btn-addbook', this.handleDeployAddBook)
    $(document).on('click', '.btn-adduser', this.handleDeployAddUser)
    $(document).on('click', '.btn-return', this.handleReturn)
    $(document).on('click', '.btn-rent', this.handleRent)
    $(document).on('click', '.btn-sendISBN', this.handleAddBook)
  },

  handleRent: function (e) {
    e.preventDefault()
    const itemId = parseInt($(e.target).data('id'))
    web3.eth.getAccounts((error, accounts) => {
      if (error) {
        console.log(error)
      }
      const account = accounts[0]
      Adoption.deployed()
        .then(instance => {
          return instance.rentBook(itemId, { from: account })
        })
        .then(result => {
          console.log(result)
          iziToast.success({
            title: 'success',
            message: 'この本をレンタルするトランザクションを生成しました！',
            position: 'topRight'
          })
          App.slackRent(itemId, account)
        })
        .catch(err => {
          console.log(err.message)
        })
    })
  },

  handleReturn: function (e) {
    e.preventDefault()
    const itemId = parseInt($(e.target).data('id'))
    web3.eth.getAccounts((error, accounts) => {
      if (error) {
        console.log(error)
      }
      const account = accounts[0]
      console.log(itemId)
      Adoption.deployed()
        .then(instance => {
          return instance.returnBook(itemId, { from: account, gas: 60000 })
        })
        .then(result => {
          console.log(result)
          iziToast.success({
            title: 'success',
            message: 'この本を返却するトランザクションを生成しました！',
            position: 'topRight'
          })
          App.slackReturn(itemId, account)
        })
        .catch(err => {
          console.log(err.message)
        })
    })
  },

  handleAddBook: function (e) {
    e.preventDefault()
    const isbn = $('#InputBookISBN').val()
    axios
      .get('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn.toString())
      .then(response => {
        const { totalItems, items } = response.data
        if (totalItems !== 1) {
          iziToast.error({ title: 'error', message: 'この本は登録できません', position: 'topRight' })
        } else {
          for (let item of items) {
            const { title } = item.volumeInfo
            $('#InputBookTitle').val(title)
            $('.btn-addbook').attr('disabled', false)
            iziToast.success({ title: 'success', message: '本の情報を取得しました', position: 'topRight' })
          }
        }
      })
      .catch(function (error) {
        console.log(error)
      })
  },

  handleDeployAddUser: function (e) {
    e.preventDefault()
    const name = $('#InputUserName').val()
    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error)
      }
      const account = accounts[0]
      User.deployed()
        .then(instance => {
          return instance.addUser(name, {
            from: account
          })
        })
        .then(result => {
          console.log(result)
          iziToast.success({
            title: 'success',
            message: 'あなたのユーザーを登録するトランザクションを生成しました！',
            position: 'topRight'
          })
        })
        .catch(err => {
          console.log(err.message)
        })
    })
  },

  handleDeployAddBook: function (e) {
    e.preventDefault()
    const isbn = $('#InputBookISBN').val()
    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error)
      }
      const account = accounts[0]
      Adoption.deployed()
        .then(instance => {
          return instance.addBooks(isbn.toString(), {
            from: account
          })
        })
        .then(result => {
          console.log(result)
          iziToast.success({
            title: 'success',
            message: 'この本を登録するトランザクションを生成しました！',
            position: 'topRight'
          })
        })
        .catch(err => {
          console.log(err.message)
        })
    })
  },

  initBooks: function () {
    Adoption.deployed()
      .then(instance => {
        return instance.getBooksLength()
      })
      .then(result => {
        const booksLength = result['c'][0]
        for (let i = 0; i < booksLength; i++) {
          setInterval(this.getBookData(i), 1000)
        }
      })
      .catch(err => {
        console.log(err)
      })
  },

  getBookDataNew: function (index) {
    return Adoption.deployed()
      .then(instance => {
        return instance.getBook(index)
      })
      .then(result => {
        return Promise.resolve([result[0], result[1], result[2], index])
      })
      .catch(err => {
        console.log(err)
      })
  },

  // TODO: そのうち上のfunctionを使うので廃棄する
  getBookData: function (index) {
    Adoption.deployed()
      .then(instance => {
        return instance.getBook(index)
      })
      .then(result => {
        this.getBookDetail(result[0], result[1], result[2], index)
      })
      .catch(err => {
        console.log(err)
      })
  },

  initCheckUser: async function(myAccount) {
    const name = await App.getUser(myAccount)
    if (name !== '') {
      $('#InputUserName').val(name)
      $('#InputUserName').prop('disabled', true)
      $('.btn-adduser').prop('disabled', true)
    } else {
      iziToast.error({
        title: 'error',
        message: 'User登録をしてください',
        position: 'topRight'
      })
    }
  },

  getUser: function (address) {
    return User.deployed()
      .then(instance => {
        return instance.getUser(address)
      })
      .then(result => {
        return Promise.resolve(result)
      })
      .catch(err => {
        console.log(err)
      })
  },

  getBookDetailNew: function (isbn) {
    return axios
      .get('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn)
      .then(response => {
        const { items } = response.data
        const item = items[0]
        const { title, authors, imageLinks, previewLink } = item.volumeInfo
        return Promise.resolve({ title, authors, imageLinks, previewLink })
      })
      .catch(function (error) {
        console.log(error)
      })
  },

  // TODO: これもそのうち廃棄
  getBookDetail: function (isbn, lend, borrow, index) {
    axios
      .get('https://www.googleapis.com/books/v1/volumes?q=isbn:' + isbn)
      .then(response => {
        const { items } = response.data
        const item = items[0]
        const { title, authors, imageLinks, previewLink } = item.volumeInfo
        this.displayBookData(title, authors, imageLinks, previewLink, lend, borrow, index)
      })
      .catch(function (error) {
        console.log(error)
      })
  },

  getSlackUser: function () {
    return axios.get(env.SLACK_USER_LIST_URL).then(response => {
      return Promise.resolve(response)
    })
  },

  slackRent: async function(index, account) {
    const bookData = await App.getBookDataNew(index)
    const bookDetail = await App.getBookDetailNew(bookData[0])
    const rentName = await App.getUser(bookData[1])
    const borrowName = await App.getUser(account)
    const text =
      '<@' + borrowName + '> さんが' + ' <@' + rentName + '> さんの `' + bookDetail.title + '` をレンタルしました'
    App.slackPost(text)
  },

  slackReturn: async function(index, account) {
    const bookData = await App.getBookDataNew(index)
    const bookDetail = await App.getBookDetailNew(bookData[0])
    const rentName = await App.getUser(bookData[1])
    const borrowName = await App.getUser(account)
    const text =
      '<@' + borrowName + '> さんが' + ' <@' + rentName + '> さんの `' + bookDetail.title + '` を返却しました'
    App.slackPost(text)
  },

  slackPost: function (text) {
    $.ajax({
      type: 'post',
      url: env.SLACK_POST_URL,
      data: {
        payload: JSON.stringify({
          link_names: true,
          text: text
        })
      },
      scriptCharset: 'utf-8'
    }).then(
      function (data) {
        iziToast.info({
          title: 'info',
          message: 'Slack #book-rental チャンネルで通知しました',
          position: 'topRight'
        })
      },
      function () {
        iziToast.error({
          title: 'error',
          message: 'Slackでの通知に失敗しました',
          position: 'topRight'
        })
      }
    )
  },

  displayBookData: function (title, authors, imageLinks, previewLink, lend, borrow, index) {
    const booksRow = $('#booksRow')
    const bookTemplate = $('#bookTemplate')
    bookTemplate.find('.book-title').text(title)
    bookTemplate.find('img').attr('src', imageLinks.smallThumbnail)
    bookTemplate.find('.book-author').text(authors)
    bookTemplate.find('.book-previewLink').attr('href', previewLink)
    bookTemplate.find('.book-lend').text(lend)
    bookTemplate.find('.btn-rent').attr('data-id', index)
    bookTemplate.find('.btn-return').attr('data-id', index)
    if (borrow !== '0x0000000000000000000000000000000000000000') {
      bookTemplate.find('.btn-rent').prop('disabled', true)
      bookTemplate.find('.btn-return').show()
    } else {
      bookTemplate.find('.btn-rent').prop('disabled', false)
      bookTemplate.find('.btn-return').hide()
    }
    booksRow.append(bookTemplate.html())
  }
}

window.addEventListener('load', function () {
  // Checking if Web3 has been injected by the browser (Mist/MetaMask)
  if (typeof web3 !== 'undefined') {
    console.warn(
      "Using web3 detected from external source. If you find that your accounts don't appear or you have 0 MetaCoin, ensure you've configured that source properly. If using MetaMask, see the following link. Feel free to delete this warning. :) http://truffleframework.com/tutorials/truffle-and-metamask"
    )
    // Use Mist/MetaMask's provider
    window.web3 = new Web3(web3.currentProvider)
  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:9545. You should remove this fallback when you deploy live, as it's inherently insecure. Consider switching to Metamask for development. More info here: http://truffleframework.com/tutorials/truffle-and-metamask"
    )
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    window.web3 = new Web3(new Web3.providers.HttpProvider('http://127.0.0.1:9545'))
  }
  App.start()
})
