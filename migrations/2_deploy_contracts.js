var Adoption = artifacts.require('Adoption')
var User = artifacts.require('User')

module.exports = function (deployer) {
  deployer.deploy(Adoption)
  deployer.deploy(User)
}
