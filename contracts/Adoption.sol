pragma solidity ^0.4.17;

contract Adoption {

    struct Book {
        string isbn;
        address lend; // 貸す人のaddress
        address borrow; // 借りる人のaddress
    }

    Book[] public books;
    event RentMessage ( uint indexed id, address lend, address borrow);
    event ReturnMessage ( uint indexed id, address lend, address borrow);

    constructor() public {
        books.push(Book("9784798151342", 0x0000000000000000000000000000000000000000, 0x0000000000000000000000000000000000000000));
        books.push(Book("9784800712059", 0x0000000000000000000000000000000000000000, 0x0000000000000000000000000000000000000000));
        books.push(Book("9784822254001", 0x0000000000000000000000000000000000000000, 0x0000000000000000000000000000000000000000));
        books.push(Book("9784798151632", 0x0000000000000000000000000000000000000000, 0x0000000000000000000000000000000000000000));
    }

    function addBooks(string isbn) public returns (uint){
        return books.push(Book(isbn, msg.sender, 0x0000000000000000000000000000000000000000)) - 1;
    }

    function getBook(uint index) public view returns (string, address, address){
        return (books[index].isbn, books[index].lend, books[index].borrow);
    }

    function rentBook(uint index) public returns (uint){
        require(books[index].borrow == 0x0000000000000000000000000000000000000000);
        books[index].borrow = msg.sender;
        emit RentMessage(index, books[index].lend, msg.sender);
        return index;
    }

    function returnBook(uint index) public returns (uint){
        require(books[index].borrow == msg.sender);
        books[index].borrow = 0x0000000000000000000000000000000000000000;
        emit ReturnMessage(index, books[index].lend, msg.sender);
        return index;
    }

    function getBooksLength() public view returns (uint){
        return books.length;
    }
}