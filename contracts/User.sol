pragma solidity ^0.4.17;

contract User {

    mapping (address => string) public users;

    function addUser(string name) public returns (string){
        users[msg.sender] = name;
        return name;
    }

    function getUser(address add) public view returns (string){
        return users[add];
    }
}