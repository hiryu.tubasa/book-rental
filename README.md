# Book Rental

## 利用ライブラリ

**Ethereum**

- [solidity](http://solidity.readthedocs.io/en/v0.4.24/)
- [truffle](https://truffleframework.com/)
- Ganache

**frontend**

- webpack 2
- bootstrap 4
- [iziToast](http://izitoast.marcelodolce.com/)
- jquery
- popper.js

**ISBN の取得**

[Google Book API](https://developers.google.com/books/)を使用しています。
使いすぎると制限かけられます。

## 環境設定

### node.js のインストール

```
$ node -v
v8.9.4
$ npm -v
5.6.0
```

### truffle のインストール

```
$ npm install -g truffle
```

### Ganache のインストール

[download](https://truffleframework.com/ganache)

起動しっぱなしにしておく

### MetaMask のインストール

[download](https://metamask.io/)

アカウント登録したら Custom RPC を選択し、`http://127.0.0.1:7545`を入力して Save

### ローカルで動作させる

```
$ cd book-rental
$ truffle compile
# 初回
$ triffle migrate
# 2回目以降は
$ truffle migrate --reset
$ npm run dev
```

http://localhost:8080 をブラウザで開く

metamask を開き、Ganache から適当なアカウントを import する
<img src="Ganache.png" alt="drawing" style="width:400px"/>
